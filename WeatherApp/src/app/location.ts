export class Location {

  constructor (
    private cityName: string,
    private zipCode: string
  ) {

  }


  public getCity(): string {
    return this.cityName;
  }

  public getZip(): string {
    return this.zipCode;
  } 

}
