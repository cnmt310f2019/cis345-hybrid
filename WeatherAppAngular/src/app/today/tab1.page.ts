import {Component, OnDestroy, OnInit} from '@angular/core';

import {WeatherService} from '../core/api/weather.service';
import {Subscription} from 'rxjs';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Weather} from '../shared/models/weather';
import {ProfileService} from '../core/api/profile.service';
import {Settings} from '../shared/models/settings';
import {LookupType} from '../shared/models/LookupType';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit, OnDestroy {

  private subscription: Subscription;
  private dataSubscription: Subscription;
  private location: Coordinates;
  private settings: Settings;
  public weather: Weather;

  constructor(private weatherService: WeatherService,
              private geoLocation: Geolocation,
              private profileService: ProfileService) {
      this.subscription = new Subscription();
  }

  ngOnInit() {
      console.log('tab1');
      this.refreshScreen();
  }

  refreshScreen(event?){
      this.subscription.add(
          this.profileService.get().subscribe((settings) => {
              this.settings = settings || new Settings();
              this.refresh();

              setTimeout(() => {
                  if (event) {
                      event.target.complete();
                  }
              }, 2000);

          })
      );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  refresh() {
      console.log('Refreshing...');
      // Will not work in emulator because of fake gps system in emulator.
      this.geoLocation.getCurrentPosition().then((resp) => {
          this.location = resp.coords;
      }).catch((error) => {
          console.log('ERROR: ' + error);
      }).finally(() => {
          this.subscription.remove(this.dataSubscription);

          if (this.settings.value == null || this.settings.value === "") {
              this.settings = new Settings({type: LookupType.Coordinates, value: this.location.longitude + ',' + this.location.latitude});
              this.profileService.save(this.settings).then(() => {
              }).then(() => {
                  this.dataSubscription = this.getData();
              });
          } else {
              this.dataSubscription = this.getData();
          }

          this.subscription.add(this.dataSubscription);
      });


  }

  getData() {
      return (this.weatherService.currentWeather(this.settings.type, this.settings.value)
          .subscribe((data) => {
              this.weather = data;
          }));
  }
}
