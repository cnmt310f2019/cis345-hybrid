import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../core/user.service';
import { Observable, Subscription } from 'rxjs';
import { User } from '../shared/models/user';
import { NavController } from '@ionic/angular';
import { ProfileService } from '../core/profile.service';
import { Settings } from '../shared/models/settings';
import { Coordinates } from '@ionic-native/geolocation/ngx';

import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit, OnDestroy {
    readonly DefaultContactLimit: number = 10;
    readonly MaximumContactLimit = 2500;
    private users: User[];
    private range = '0';
    private subscription: Subscription;
    private dataSubscription: Subscription;
    private settings: Settings;
    private location: Coordinates;

    constructor(private userServer: UserService,
        private profileService: ProfileService,
        private geoLocation: Geolocation,
        private navController: NavController) {

        this.subscription = new Subscription();

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    handleFilter(event: Event) {

        this.refresh();
    }

    applyRangeFilter(users: Array<User>, limit?: number, location?: Coordinates, range?: number): Array<User> {
        range = range || parseInt(this.range, 10);
        location = location || this.location;
        limit = limit || this.settings.contactLimit || this.DefaultContactLimit;

        if (range && location) {
            users = users.filter((user) => {
                return (user.address.distanceFrom(location.latitude, location.longitude) <= range);
            });
        }

        return (users.slice(0, limit));
    }

    displayDistance(distance: number) {
        return (parseInt(String(distance), 10));
    }

    refresh(limit?: number) {
        // Will not work in emulator because of fake gps system in emulator.
        this.geoLocation.getCurrentPosition().then((resp) => {
            this.location = resp.coords;

        }).catch((error) => {
            console.log('ERROR: ' + error);
        }).finally(() => {
            this.subscription.remove(this.dataSubscription);

            // if limiting to a range, grab first 2500 results
            // Filter on client side
            const range: number = parseInt(this.range, 10);
            const adjustedLimit: number = (!range) ? (limit || this.settings.contactLimit || this.DefaultContactLimit) : this.MaximumContactLimit;
            this.dataSubscription = this.userServer.all(adjustedLimit).subscribe((users) => {
                this.users = this.applyRangeFilter(users, limit, this.location, range);
            });

            this.subscription.add(this.dataSubscription);
        });    
    }

    ngOnInit() {
        this.subscription.add(
            this.profileService.get().subscribe((settings) => {

            this.settings = settings || new Settings();

                this.refresh(this.settings.contactLimit);
            })
        );
    }

    displayUserDetail(user: User) {
        this.navController.navigateForward(['/detail/' + user.id], {
            state: user
        });
    }
}
