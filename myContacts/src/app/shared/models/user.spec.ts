import { User } from './user';

describe('User', () => {
  it('should create an instance', () => {
    expect(new User()).toBeTruthy();
  });


  describe('name', () =>{
    it('should return last, first', () => {
      const user: User =  new User();
      user.firstName = 'Ariadna';
      user.lastName = 'da Paz';

      expect(user.name).toEqual('da Paz, Ariadna');

    });
   });

  describe('fromJson', () => {
    const json =  {
      gender: 'female',
      name: {
        title: 'Miss',
        first: 'Ariadna',
        last: 'da Paz'
      },
      location: {
        street: {
          number: 9377,
          name: 'Rua Vinte de Setembro'
        },
        city: 'Maricá',
        state: 'São Paulo',
        country: 'Brazil',
        postcode: 68275,
        coordinates: {
          latitude: '-72.0977',
          longitude: '54.1531'
        },
        timezone: {
          offset: '-11:00',
          description: 'Midway Island, Samoa'
        }
      },
      email: 'ariadna.dapaz@example.com',
      login: {
        uuid: '1430fe8e-ac9f-4b8c-ae65-c2f0bb7aed12',
        username: 'goldenpeacock216',
        password: 'grand',
        salt: 'VM9UIW34',
        md5: '713229e54f88bf2e8ad3b02c5b320f9e',
        sha1: '12ddf267ef6c5645fef498abb546f1da9d13371a',
        sha256: 'a4e8ecc3419f402459578213179a344b8edf8755bedb3f5ca09a8322f08be605'
      },
      dob: {
        date: '1998-05-06T20:59:04.876Z',
        age: 22
      },
      registered: {
        date: '2002-12-30T02:08:07.328Z',
        age: 18
      },
      phone: '(64) 6981-1198',
      cell: '(66) 5844-4869',
      id: {
        name: '',
        value: null
      },
      picture: {
        large: 'https://randomuser.me/api/portraits/women/68.jpg',
        medium: 'https://randomuser.me/api/portraits/med/women/68.jpg',
        thumbnail: 'https://randomuser.me/api/portraits/thumb/women/68.jpg'
      },
      nat: 'BR'
    };

    it('with null json returns null', () => {
      const user: User = User.fromJson(null);
      expect(user).toBeFalsy();
    });

    it('should assign all properties', () =>{
      const user: User = User.fromJson(json);

      expect(user.id).toEqual('1430fe8e-ac9f-4b8c-ae65-c2f0bb7aed12');
      expect(user.firstName).toEqual('Ariadna');
      expect(user.lastName).toEqual('da Paz');
        expect(user.email).toEqual('ariadna.dapaz@example.com');
        expect(user.photo).toEqual('https://randomuser.me/api/portraits/women/68.jpg');
        expect(user.address).toBeTruthy();
    });

  });

});
