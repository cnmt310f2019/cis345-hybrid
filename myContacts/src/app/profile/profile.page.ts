import { Component, OnInit, OnDestroy } from '@angular/core';
import { Settings } from '../shared/models/settings';
import { ProfileService } from '../core/profile.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit, OnDestroy {

    readonly DefaultPhoto: string = './assets/camera.png';
    private settings: Settings;
    private subscription: Subscription;

    constructor(private profileService: ProfileService) {
        this.settings = this.getDefaultSettings();
        this.subscription = new Subscription();
    }

    ngOnInit() {
        this.subscription.add(
            this.profileService.get().subscribe((settings) => {
                this.settings = settings || this.getDefaultSettings();
            })
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    getDefaultSettings() {
        return (new Settings({
            photo: this.DefaultPhoto
        }));
    }

    handleSave(event: any) {
        this.profileService.save(this.settings).then((settings) => {
            this.settings = settings || this.getDefaultSettings();
        }).catch((error) => {
            // display error message
            console.log(error);
        });
    }

    handlePhoto(event: MouseEvent) {
        if (this.profileService.isCameraAvailable()) {
            this.profileService.takePhoto().then((photo) => {
    
                this.settings.photo = 'data:image/jpeg;base64,' + photo;
            });
        } else {
            console.log('Camera not available');
        }
    }
}
