import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../core/user.service';
import { NavController, AlertController } from '@ionic/angular';
import { User } from '../shared/models/user';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit, OnDestroy {

    private user: User;
    private id: string;
    private location: Coordinates;
    private subscription: Subscription;

    constructor(private route: ActivatedRoute, private router: Router,
        private userService: UserService,
        private navCtlr: NavController,
        private alertController: AlertController,
        private geoLocation: Geolocation) {

        this.subscription = new Subscription();

        this.subscription.add(
            this.route.params.subscribe(params => {
                this.id = params.id;

                if (this.router.getCurrentNavigation().extras.state) {
                    this.user = this.router.getCurrentNavigation().extras.state as User;
                }
            })
        );
    }

    ngOnDestroy() {
        this.subscription
    }

    ngOnInit() {
      // if the user wasn't loaded, then load via service

        if (!this.user) {
            this.subscription.add(
                this.userService.get(this.id).subscribe((user) => {
                    this.user = user;
                })
            );
        }

        this.geoLocation.getCurrentPosition().then((resp) => {
            this.location = resp.coords;
        }).catch((error) => {
            this.location = null;
            console.log('Failed to get current location. Error: ' + error);
        });
    }

    showMap(latitude?: number, longitude?: number) {
        this.alertController.create({
            header: 'To Do',
            subHeader: 'Display Map',
            message: 'Display the map for lat: ' + (latitude || this.user.address.latitude) +
                ', long: ' + (longitude || this.user.address.longitude),
            buttons: ['OK']
        }).then((alert) => {
            alert.present();
        });
    }

}
