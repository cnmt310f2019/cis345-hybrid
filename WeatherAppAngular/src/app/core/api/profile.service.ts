import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import {from, Observable} from 'rxjs';
import {Settings} from '../../shared/models/settings';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  readonly ProfileSettingName: string = 'profileWeather';
  constructor(private storage: Storage) { }

  get(): Observable<Settings> {
    return from(this.storage.get(this.ProfileSettingName));
  }

  save(settings: Settings): Promise<any> {
    return this.storage.set(this.ProfileSettingName, settings);
  }
}
