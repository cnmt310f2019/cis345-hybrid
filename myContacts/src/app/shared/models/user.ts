import { Address } from './address';

export class User {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    photo: string;

    address: Address;

    get name(): string{
        return (this.lastName + ', ' + this.firstName);
    }

    static fromJson(json: any){
        if(json == null){
            return null;
        }

        return(Object.assign(Object.create(User.prototype), {
            id: json.login.uuid,
            firstName: json.name.first,
            lastName: json.name.last,
            email: json.email,
            photo: json.picture.large,
            address: Address.fromJson(json.location)
        }));
    }
}
