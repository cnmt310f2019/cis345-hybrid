import { Component } from '@angular/core';
import { Location } from './location';
import { WeatherServiceService } from './weather-service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'WeatherApp';

  locations: Location[] = [
    new Location("New York City", "10001"),
    new Location("Seattle", "98101"),
    new Location("Phoenix", "85001"),
    new Location("Miami", "33101"),
    new Location("Stevens Point", "54481"),
  ];

  weatherData: any[];

  constructor(private weatherService: WeatherServiceService) { }

  ngOnInit() {
    this.getWeather();
  }


  getWeather() {
    this.weatherData = [];
     this.locations.forEach((loc) => {
       this.weatherService.getWeatherData(loc.getZip()).subscribe(data => {
         this.weatherData.push(data);
       });
     });

     console.log(this.weatherData);
  }
}
