import { Component, OnInit } from '@angular/core';
import { NavController, AlertController} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import {Weather} from '../shared/models/weather';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  weatherDetail: { [p: string]: any };

  constructor( private navCtlr: NavController,
               private alertController: AlertController,
               private router: Router) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.weatherDetail = this.router.getCurrentNavigation().extras.state;
    }
  }

  ngOnInit() {

  }

  displayWeatherDetail(weather: Weather) {

    const message =
        '<p>Wind Speed: ' + weather.windSpeed +
        '</p><p>Wind Direction: ' + weather.windDirection +
        '</p>' +
        (weather.rainVolume ? '<p>Rain Volume: ' + weather.rainVolume + '</p>' : '') +
        (weather.snowVolume ? '<p>Snow Volume: ' + weather.snowVolume + '</p>' : '');


    this.alertController.create({
      header: 'Details',
      subHeader: weather.dateTime.toLocaleTimeString(),
      message,
      backdropDismiss: false,
      buttons: ['Close']
    }).then((alert) => {
      alert.present();
    });
  }

}
