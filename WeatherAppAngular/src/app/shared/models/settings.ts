import {LookupType} from './LookupType';

export class Settings {
    type: LookupType;
    value: string;

    constructor(settings?: {type: LookupType, value: string}) {
        if(settings) {
            Object.assign(this, settings);
        }
    }
}
