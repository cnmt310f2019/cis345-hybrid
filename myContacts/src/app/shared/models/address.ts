export class Address {
    street: string;
    city: string;
    state: string;
    zipCode: string;
    country: string;
    latitude: number;
    longitude: number;


    static fromJson(json: any) {
        if (json == null) {
            return null;
        }

        return (Object.assign(Object.create(Address.prototype), {
            street: json.street.number + ' ' + json.street.name,
            city: json.city,
            state: json.state,
            country: json.country,
            zipCode: String(json.postcode),
            latitude: Number(json.coordinates.latitude),
            longitude: Number(json.coordinates.longitude)
        }));
    }

    distanceFrom(latitude: number, longitude: number): number {
        const kilometersToMiles = 0.621371;
        const degreeToRadian = Math.PI / 180.0;
        const earthRadius = 6371.0;

        const a = 0.5 - Math.cos((this.latitude - latitude) * degreeToRadian) / 2 +
            Math.cos(this.latitude * degreeToRadian) * Math.cos((latitude) * degreeToRadian) *
            (1 - Math.cos(((this.longitude - longitude) * degreeToRadian))) / 2;

        const distance = ((earthRadius * 2) * Math.asin(Math.sqrt(a)));

        return distance * kilometersToMiles;
    }
}
