import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {User} from '../shared/models/user';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly  baseUrl: string = 'https://randomuser.me/api/';
    private readonly seed: string = 'uwsp';

    private data: Observable<User[]>;

  constructor(private  httpClient: HttpClient) { }

  public all(limit: number = 100): Observable<any>{
    const url: string = this.baseUrl + '?nat=US&seed=' + this.seed +
          '&results=' + limit;

    console.log('UserService.all(): HTTP GET "' + url + '"');

    return this.httpClient.get(url).pipe(
        map((data: any) => {
          const users: Array<User> = new Array<User>();
          data.results.forEach((item) => {
              // parse item/Josn into a user
              const user: User = User.fromJson(item);
              if(user != null){
                  users.push(user);
              }
          });

            this.data = of(users);

          return users;
        })
    );
  }

    public get(id: string): Observable<User> {
        const url: string = this.baseUrl + '?nat=US&seed=' + this.seed;
        console.log('UserService.get(' + id + '): HTTP GET "' + url + '"');

        return ((this.data || this.all()).pipe(
            map((users) => users.find((u) => u.id === id))
        ))
    }
}
