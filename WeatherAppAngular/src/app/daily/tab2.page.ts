import {Component, OnDestroy, OnInit} from '@angular/core';
import {Weather} from '../shared/models/weather';
import {WeatherService} from '../core/api/weather.service';
import {Subscription} from 'rxjs';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import { NavController } from '@ionic/angular';
import {retry} from 'rxjs/operators';
import {ProfileService} from '../core/api/profile.service';
import {Settings} from '../shared/models/settings';
import {LookupType} from '../shared/models/LookupType';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit, OnDestroy {

  weatherData: Weather[];
  private subscription: Subscription;
  private dataSubscription: Subscription;
  private settings: Settings;
  private location: Coordinates;

  distinctWeatherDays: { date: string; icon: string }[];

  constructor(private weatherService: WeatherService,
              private geoLocation: Geolocation,
              private navController: NavController,
              private profileService: ProfileService) {

    this.subscription = new Subscription();
  }

  ngOnInit() {
    this.refreshScreen();
  }

  refreshScreen(event?){
    this.subscription.add(
        this.profileService.get().subscribe((settings) => {
          this.settings = settings || new Settings();
          this.refresh();

          setTimeout(() => {
            if (event) {
              event.target.complete();
            }
          }, 2000);

        })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  refresh() {
    this.distinctWeatherDays = [];
    // Will not work in emulator because of fake gps system in emulator.
    this.geoLocation.getCurrentPosition().then((resp) => {
      this.location = resp.coords;
    }).catch((error) => {
      console.log('ERROR: ' + error);
    }).finally(() => {
      this.subscription.remove(this.dataSubscription);

      if (this.settings.value == null || this.settings.value === "") {
        this.settings = new Settings({type: LookupType.Coordinates, value: this.location.longitude + ',' + this.location.latitude});
        this.profileService.save(this.settings).then(() => {
          this.dataSubscription = this.getData();
        });
      } else{
        this.dataSubscription = this.getData();
      }

      this.subscription.add(this.dataSubscription);
    });
  }


  getData() {
    return (this.weatherService.fiveDay(this.settings.type, this.settings.value)
        .subscribe((data) => {
          this.weatherData = data;
          this.distinctWeatherDays = Array.from(new Set(data.map(i => i.dateTime.toDateString())))
              .map(date => {
                return {
                  date,
                  icon: data.find(x => x.dateTime.toDateString() === date).icon
                };
              });
        }));
  }

  getDayDetails(date) {
    const weatherArr: Array<Weather> = new Array<Weather>();
    this.weatherData.forEach((item) => {
      if (item.dateTime.toDateString() === date.date) {
        weatherArr.push(item);
      }
    });
    return weatherArr;
  }

  displayWeatherDetail(day) {
    this.navController.navigateForward(['/detail/' + day.date], {
      state: this.getDayDetails(day)
    });
  }

}
