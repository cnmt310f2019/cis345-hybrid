import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Weather} from '../../shared/models/weather';
import {map} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {LookupType} from '../../shared/models/LookupType';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private readonly baseUrl: string = 'http://api.openweathermap.org/data/2.5/';
  private readonly urlAPIKey: string = '?id=524901&APPID=2e13f70abbe9db0af010735a0343419c';
  private readonly tempDisplay: string = '&units=imperial';
  private weatherData: Observable<Weather[]>;
  constructor(private httpClient: HttpClient) { }

  public currentWeather(lookupType: LookupType, ...args: any[]) {
    const url: string = this.getUrl('weather', lookupType, args);

    if (url == null){
      return null;
    }
    console.log('HTTP GET: currentWeather(): ' + url);

    return this.httpClient.get(url).pipe(
        map((data: any) => {
          const weather: Weather = Weather.fromJson(data);

          return weather;
        })
    );

  }

  public fiveDay(lookupType: LookupType, ...args: any[]) {

    const url: string = this.getUrl('forecast', lookupType, args);

    if (url == null){
      return null;
    }

    console.log('HTTP GET: FiveDay(): ' + url);

    return this.httpClient.get(url).pipe(
        map((data: any) => {
          const weatherData: Array<Weather> = new Array<Weather>();
          data.list.forEach((item) => {
            const weather: Weather = Weather.fromJson(item);
            weather.cityName = data.city.name;
            if(weather != null){
              weatherData.push(weather);
            }
          });

          this.weatherData = of(weatherData);

          return weatherData;
        })
    );

  }


  private getUrl(api: string, lookupType: LookupType, ...args: any[]) {
    let url: string = this.baseUrl + api + this.urlAPIKey + '&';

    if (lookupType === LookupType.ZipCode) {
        url += 'zip=' + String(args[0]).trim();
    } else if (lookupType === LookupType.Coordinates) {
        const split = String(args[0]).split(',');
        url += 'lon=' + split[0] + '&lat=' + split[1].trim();
    } else if (lookupType === LookupType.CityAndState) {
        const split = String(args[0]).split(',');
        url += 'q=' + split[0].trim() + '&state=' + split[1].trim();
    } else {
        return null;
    }

    return (url += this.tempDisplay);
  }
}
