import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'parseInt' })
export class ParseIntPipe implements PipeTransform{
    transform(value: any, radix?: number): any {
        return parseInt(value, radix || 10);
    }
}