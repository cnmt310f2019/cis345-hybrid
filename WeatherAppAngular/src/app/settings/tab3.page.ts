import {Component, OnDestroy, OnInit} from '@angular/core';
import {Settings} from '../shared/models/settings';
import {ProfileService} from '../core/api/profile.service';
import {Subscription} from 'rxjs';
import {LookupType} from '../shared/models/LookupType';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit, OnDestroy {
  private settings: Settings;
  private subscription: Subscription;

  constructor(private profileService: ProfileService,
              private alertController: AlertController,) {
    this.settings = this.getDefaultLocationSettings();
    this.subscription = new Subscription();
  }

  ngOnInit() {
    this.subscription.add(
        this.profileService.get().subscribe((settings) => {
          this.settings = settings || this.getDefaultLocationSettings();
        })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getDefaultLocationSettings() {
    return (new Settings({
      type: LookupType.ZipCode,
      value: '54476'
    }));
  }

  handleSave(event: any) {
    this.profileService.save(this.settings).then((settings) => {
      this.settings = settings || this.getDefaultLocationSettings();
      this.alertController.create({
        header: 'Info',
        message: 'Saved Successfully',
        backdropDismiss: false,
        buttons: ['Close']
      }).then((alert) => {
        alert.present();
      });
    }).catch((error) => {
      // display error message
      console.log(error);
    });
  }
}
