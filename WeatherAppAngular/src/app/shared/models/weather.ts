export class Weather {

    cityName: string;
    highTemp: string;
    lowTemp: string;
    currentTemp: string;
    feelsLike: string;
    windSpeed: string;
    windDirection: string;
    description: string;
    icon: string;
    rainVolume: string;
    snowVolume: string;
    dateTime: Date;

    static degToCompass(num) {
        const val = Math.floor((num / 22.5) + 0.5);
        const arr = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];
        return arr[(val % 16)];
    }

    static fromJson(json: any) {
        if (json == null) {
            return null;
        }

        return(Object.assign(Object.create(Weather.prototype), {
            cityName: json.name,
            description: json.weather[0].description,
            icon: json.weather[0].icon,
            highTemp: json.main.temp_max,
            lowTemp: json.main.temp_min,
            currentTemp: json.main.temp,
            feelsLike: json.main.feels_like,
            windSpeed: json.wind.speed,
            windDirection: Weather.degToCompass(json.wind.deg),
            rainVolume: json.rain ? json.rain['3h'] : null,
            snowVolume: json.snow ? json.snow['3h'] : null,
            dateTime: json.dt_txt ? new Date(json.dt_txt) : null
        }));
    }
}
