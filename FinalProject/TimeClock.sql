USE master
GO

CREATE DATABASE TimeClock
GO

----------==================== Tables

CREATE TABLE TimeClock.dbo.Department (
ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
DepartmentName varchar(75) NOT NULL)

CREATE TABLE TimeClock.dbo.Role(
ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
RoleName varchar(50) NOT NULL
)

CREATE TABLE TimeClock.dbo.Employee (
ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
EmployeeNumber int NOT NULL,
FirstName varchar(50) NOT NULL,
LastName varchar(50) NOT NULL,
RoleID int NOT NULL,
DepartmentId int NOT NULL,
PhoneNumber varchar(11) NOT NULL,
StreetAddress varchar(100) NOT NULL,
City varchar(50) NOT NULL,
State char(2) NOT NULL
)

CREATE TABLE TimeClock.dbo.[Event](
ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
EventType varchar(25) NOT NULL
)

CREATE TABLE TimeClock.dbo.TimeLog(
ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
EmployeeID int NOT NULL,
EventID int NOT NULL,
TimeStamp datetime not null
)

CREATE TABLE TimeClock.dbo.Login(
ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
UserName varchar(50) NOT NULL,
Password varchar(max)
)


--------==================== Foreign Keys

ALTER TABLE TimeClock.dbo.Employee 
ADD CONSTRAINT FK_Role_RoleID FOREIGN KEY (RoleID)
REFERENCES TimeClock.dbo.Role(ID)

ALTER TABLE TimeClock.dbo.Employee 
ADD CONSTRAINT FK_Department_DepartmentID FOREIGN KEY (DepartmentID)
REFERENCES TimeClock.dbo.Department(ID)

ALTER TABLE TimeClock.dbo.TimeLog
ADD CONSTRAINT FK_Employee_EmployeeID FOREIGN KEY(EmployeeID)
REFERENCES TimeClock.dbo.Employee(ID)

ALTER TABLE TimeClock.dbo.TimeLog
ADD CONSTRAINT FK_Event_EventID FOREIGN KEY(EventID)
REFERENCES TimeClock.dbo.Event(ID)




INSERT INTO TimeClock.dbo.Department
([DepartmentName])
VALUES 
('IT'),
('Marketing')

INSERT INTO TimeClock.dbo.Role
(RoleName)
VALUES
('IT Intern'),
('Marketing Intern')

INSERT INTO TimeClock.dbo.Event
(EventType)
VALUES
('Clock In'),
('Clock Out')

INSERT INTO TimeClock.dbo.Employee
([EmployeeNumber], [FirstName], [LastName], [RoleId], [DepartmentId], PhoneNumber, StreetAddress, City, State)
VALUES 
('1234', 'Edward', 'Snowden', 1, 1, '7156814872', '6783 Fish Street','Weston', 'WI'),
('5678', 'Aaron', 'Rodgers', 2, 2, '7158975814', '1251 Red Ave', 'Wausau', 'WI')

INSERT INTO TimeClock.dbo.[TimeLog]
(EmployeeId, EventId, TimeStamp)
VALUES

(1, 1, DATEADD(HOUR, -8, GETDATE())),
(2, 1, DATEADD(HOUR, -7, GETDATE())),
(1, 2, GETDATE()),
(2, 2, GETDATE())


INSERT INTO TimeClock.dbo.Login
(UserName, Password)
VALUES
('test@company', 'pass')