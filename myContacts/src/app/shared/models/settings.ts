export class Settings {
    lastName: string;
    firstName: string;
    photo: string;

    contactLimit: number;

    constructor(settings?: {lastName? : string, firstName? : string, photo?: string, contactLimit?: number}) {
        if (settings) {
            Object.assign(this, settings);
        }
    }
}
