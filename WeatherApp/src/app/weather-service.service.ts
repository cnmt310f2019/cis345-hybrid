import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherServiceService {

  endpoint = "http://api.openweathermap.org/data/2.5/weather?id=524901&APPID=2e13f70abbe9db0af010735a0343419c&zip=";

  constructor(private http: HttpClient) { }


  getWeatherData(zip: string) {
    return this.http.get(this.endpoint + zip + ",us");
  }
}
